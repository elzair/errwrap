#!/bin/sh
DIRNAME=$(dirname "$(readlink -f "$0")")
BUILDDIR="$DIRNAME/build"

rm -rf $BUILDDIR
mkdir -p $BUILDDIR
cd $BUILDDIR
cmake $DIRNAME
make
