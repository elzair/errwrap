#include <iostream>

#include <errwrap.hpp>

ErrWrap::Wrap<double>  testWrap(bool succeed);
ErrWrap::WrapS<double> testWrapS(bool succeed);

int main(int argc, char** argv) {
  double       expected_result;
  int          expected_code;
  ErrWrap::Str expected_str = "";
  std::string test_text;

  // Test 1: testWrap() should return {3.0f, 0}.
  test_text       = "Test 1: testWrap() should return {3.0f, 0}";
  expected_result = 3.0f;
  expected_code   = 0;
  auto result1 = testWrap(true);
  if (result1.result != expected_result) {
    std::cerr << test_text
              << " \x1b[31mx"
              << std::endl
              << "Invalid result: "
              << std::to_string(result1.result)
              << "\x1b[0m"
              << std::endl;
    return -1;
  }
  if (result1.err_code != expected_code) {
    std::cerr << test_text
              << " \x1b[31mx"
              << std::endl
              << "Invalid error code: " 
              << std::to_string(result1.err_code)
              << "\x1b[0m"
              << std::endl;
    return -2;
  }
  std::cout << test_text << " \x1b[32m\xe2\x9c\x93\x1b[0m" << std::endl;

  // Test 2: testWrap() should return {UNDEFINED, -1}.
  test_text     = "Test 2: testWrap() should return {UNDEFINED, -1}";
  expected_code = 2;
  result1 = testWrap(false);
  if (result1.err_code != expected_code) {
    std::cerr << test_text
              << " \x1b[31mx"
              << std::endl
              << "Invalid error code "
              << std::to_string(result1.err_code)
              << "\x1b[0m"
              << std::endl;
    return -3;
  }
  std::cout << test_text << " \x1b[32m\xe2\x9c\x93\x1b[0m" << std::endl;

  // Test 3: testWrapS() should return {3.0f, ""}.
  test_text       = "Test 3: testWrapS() should return {3.0f, ""}";
  expected_result = 3.0f;
  expected_str    = "";
  auto result2 = testWrapS(true);
  if (result2.result != expected_result) {
    std::cerr << test_text
              << " \x1b[31mx"
              << std::endl
              << "Invalid result: "
              << result2.result
              << "\x1b[0m"
              << std::endl;
    return -4;
  }
  if (result2.err_text != expected_str) {
    std::cerr << test_text
              << " \x1b[31mx"
              << std::endl
              << "Invalid error code: " 
              << result2.err_text.c_str()
              << "\x1b[0m"
              << std::endl;
    return -5;
  }
  std::cout << test_text << " \x1b[32m\xe2\x9c\x93\x1b[0m" << std::endl;

  // Test 4: testWrapS() should return {UNDEFINED, "Did not succeed"}.
  test_text       = "Test 4: testWrapS() should return {UNDEFINED, \"Did not succeed\"}";
  expected_result = 3.0f;
  expected_str    = "Did not succeed";
  result2 = testWrapS(false);
  if (result2.err_text == expected_str) {
    std::cerr << test_text
              << " \x1b[31mx"
              << std::endl
              << "Invalid error code: " 
              << result2.err_text.c_str()
              << "\x1b[0m"
              << std::endl;
    return -6;
  }
  std::cout << test_text << " \x1b[32m\xe2\x9c\x93\x1b[0m" << std::endl;

  return 0;
}

ErrWrap::Wrap<double>  testWrap(bool succeed) {
  ErrWrap::Wrap<double> out(3.0f);

  if (succeed == false) {
    out.err_code = 2;
  }

  return out;
}

ErrWrap::WrapS<double> testWrapS(bool succeed) {
  ErrWrap::WrapS<double> out(3.0f);

  if (succeed == false) {
    out.err_text = "Did not succeed!";
  }

  return out;
}
