#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <errwrap.hpp>

namespace ErrWrap {
  /**
   * This constructor copies at most MAX_ERRSTR_LEN number of characters
   * from the input character array into the new object's internal array.
   * @param str the input char array
   */
  Str::Str(const char* str) {
    memset(this->data, 0, MAX_ERRSTR_LEN);
    this->insert(str, 0);
  }
  
  /**
   * This is the default constructor for the Str class.
   */
  Str::Str() {
    memset(this->data, 0, MAX_ERRSTR_LEN);
  }
  
  /**
   * This function returns the length of the current string.
   * Specifically, it returns the number of chars before
   * the first NULL (i.e. '\0').
   * @return the length of the string
   */
  size_t Str::length() {
    return this->len;
  }
  
  /**
   * This function returns a pointer to the internal
   * character array used by the object.
   * @return the object's char array
   */
  const char* Str::c_str() {
    return this->data;
  }
   
  /**
   * This function inserts into the specified index of the object's
   * internal character array the contents of the given character array.
   * @param str the input char array
   * @param idx the index to start insertion
   */
  void Str::insert(const char *str, size_t idx) {
    auto stlen = strlen(str);
    assert((idx + stlen) < MAX_ERRSTR_LEN);
    this->len = idx + stlen;
    memset(&this->data[idx], 0, stlen);
    strncpy(&this->data[idx], str, MAX_ERRSTR_LEN - idx);
  }
   
  /**
   * This operator copies into the left Str's internal storage
   * the contents of the right character array.
   * @param str the right character array
   * @return a reference to the left Str
   */
  Str& Str::operator=(const char* str) {
    memset(this->data, 0, MAX_ERRSTR_LEN);
    this->insert(str);
    return *this;
  }
  
  /**
   * This operator copies into the left Str's internal storage
   * the contents of the right Str's character array.
   * @param str the right character array
   * @return a reference to the left Str
   */
  Str& Str::operator=(Str str) {
    memset(this->data, 0, MAX_ERRSTR_LEN);
    this->insert(str.c_str());
    return *this;
  }
  
  /**
   * This operator copies into the object's internal storage 
   * the contents of the right character array.
   * @param str the right character array
   * @return a reference to the left Str
   */
  Str& Str::operator+=(const char* str) {
    this->insert(str, this->len);
    return *this;
  }
  
  /**
   * This operator copies into the object's internal storage 
   * the contents of the right Str's character array.
   * @param str the right character array
   * @return a reference to the left Str
   */
  Str& Str::operator+=(Str str) {
    this->insert(str.data, this->len);
    return *this;
  }
  
  /**
   * This operator creates a new Str with the contents of both
   * the old Str and the character array.
   * @param str the right character array
   * @return a reference to the left Str
   */
  Str Str::operator+(const char* str) {
    Str out  = *this;
    out        += str;
    return out;
  }
  
  /**
   * This operator creates a new Str with the contents of both
   * the left Str's and the right Str's character array.
   * @param str the right Str
   * @return the new Str
   */
  Str Str::operator+(Str str) {
    Str out  = *this;
    out        += str;
    return out;
  }
  
  /**
   * This operator determines if the contents of the left Str's internal
   * character array match the contents of the right character array.
   * @param str the right character array
   * @return true if the two arrays match, false if they do not
   */
  bool Str::operator==(const char* str) {
    return this->isEqual(str);
  }
  
  /**
   * This operator determines if the contents of the left Str's internal
   * character array match the contents of the right Str's internal
   * character array.
   * @param str the right Str 
   * @return true if the two Strs match, false if they do not
   */
  bool Str::operator==(const Str str) {
    return this->isEqual(str);
  }
  
  /**
   * This operator determines if the contents of the left Str's internal
   * character array does not match the contents of the right character array.
   * @param str the right character array
   * @return true if the two arrays do not match, false if they do
   */
  bool Str::operator!=(const char* str) {
    auto opposite_result = this->isEqual(str);
  
    return opposite_result == true ? false : true; // Return opposite of ==
  }
  
  /**
   * This operator determines if the contents of the left Str's internal
   * character array does not match the contents of the right Str's internal
   * character array..
   * @param str the right character array
   * @return true if the two arrays do not match, false if they do
   */
  bool Str::operator!=(const Str str) {
    auto opposite_result = this->isEqual(str);
  
    return opposite_result == true ? false : true; // Return opposite of ==
  }
  
  /**
   * This is a helper function for the == and != operators.
   * It determines if the contents of the Str's internal
   * character array match the contents of the input character array.
   * @param the input character array
   * @return true if the two arrays match, false if they do not
   */
  bool Str::isEqual(const char* str) {
    auto stlen = strlen(str);
  
    if (stlen != this->len) {
      return false;
    }
  
    auto result = strncmp(this->data, str, this->len);
  
    return result == 0 ? true : false;
  }
  
  /**
   * This is a helper function for the == and != operators.
   * It determines if the contents of the Str's internal
   * character array match the contents of another Str.
   * @param the input character array
   * @return true if the two arrays match, false if they do not
   */
  bool Str::isEqual(Str str) {
    auto stlen = strlen(str.c_str());
  
    if (stlen != this->len) {
      return false;
    }
  
    auto result = strncmp(this->data, str.c_str(), this->len);
  
    return result == 0 ? true : false;
  }
  
  
  /**
   * This function converts an integer to its base-16 character representation.
   * @param num the input number
   * @return an Str containing the number's string representation
   */
  Str num2hexstr(uint64_t num) {
    Str str = "0x";
    
    for (auto i = 0; i < 16; i++) {
      uint8_t res = ((num >> (64 - 4 * (i + 1))) & 0b1111);
      char cstr[2] = "";
  
      assert(res < 16);
  
      if (res < 10) {
        cstr[0] = (char)(res + 0x30);      // 0x30 = '0' in ASCII
        cstr[1] = '\0';
      }
      else {
        cstr[0] = (char)(res - 10 + 0x61); // 0x61 = 'A' in ASCII
        cstr[1] = '\0';
      }
  
      str.insert(cstr, i+2);
    }
  
    return str;
  }
}
