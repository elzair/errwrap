#ifndef __EITHER_HPP__
#define __EITHER_HPP__

//#include <errstr.hpp>
#include <cstddef>
#include <cstdint>

namespace ErrWrap {
  /**
   * This is the maximum number of characters for an Str string.
   */
  const size_t MAX_ERRSTR_LEN = 100;
  
  /**
   * This is a simple string class meant for use with Either to report errors.
   * It has a fixed size of MAX_ERRSTR_LEN.
   */
  class Str {
  public:
  
    Str(const char* str);
    
    Str();
  
    size_t length();
  
    const char* c_str(); 
      
    void insert(const char *str, size_t idx = 0); 
    
    Str& operator=(const char* str);   
  
    Str& operator=(const Str str); 
  
    Str& operator+=(const char* str); 
  
    Str& operator+=(Str str); 
  
    Str operator+(const char* str); 
  
    Str operator+(Str str); 
  
    bool operator==(const char* str); 
    
    bool operator==(const Str str); 
  
    bool operator!=(const char* str); 
  
    bool operator!=(const Str str); 
  
  private:
    /**
     * The length of the Str's character array.
     */
    size_t len = 0;
  
    /**
     * The Str's internal character array.
     */
    char   data[MAX_ERRSTR_LEN];
  
    bool isEqual(const char* str);
  
    bool isEqual(const Str str);
  
  };
  
  Str num2hexstr(uint64_t num);
  
  /**
   * This struct is used to return either the result of a function or an error code.
   */
  template<class T>
  struct Wrap {
    /**
     * The result of a successful function application.
     */
    T   result;
  
    /**
     * The error code (default: 0)
     */
    int err_code = 0;

    /*
     * This is the constructor function for initializing all of the fields of Wrap.
     * @param result result of a successful function application
     * @param err_code error code
     */
    Wrap(T in_result, int in_err_code) : result(in_result),
                                         err_code(in_err_code) {}

    /*
     * This is the constructor function for initializing all of the fields of Wrap.
     * @param result result of a successful function application
     */
    Wrap(T in_result) : result(in_result) {}
  };

  /**
   * This struct is used to return either the result of a function or an error string.
   */
  template<class T>
  struct WrapS {
    /**
     * The result of a successful function application.
     */
    T   result;
  
    /**
     * A string containing Error information.
     */
    Str err_text = "";
    
    /*
     * This is the constructor function for initializing all of the fields of WrapS.
     * @param result result of a successful function application
     * @param err_text error information 
     */
    WrapS(T in_result, char* in_err_text) : result(in_result),
                                            err_text(in_err_text) {}

    /*
     * This is the constructor function for initializing all of the fields of WrapS.
     * @param result result of a successful function application
     */
    WrapS(T in_result) : result(in_result) {}
  };
}

#endif
